#include "functions.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

void print_pop( int ** pop, int pop_size)
{
	int i,j;
	for (i=0; i<pop_size; i++)
	{
		for (j=0; j<NUM_INTS; j++)
		{
			printf("%d ", pop[i][j]);
		}
		printf("\n");
	}
	
}

//keep record of the outcome of the last two games
void update_hist(int ** hist, int ** pop, int i, int j)
{
	int bit, bit_loc;
	bit = hist[i][0]*pow(2,3) + hist[j][0]*pow(2,2) + hist[i][1]*2 + hist[j][1]*1;
	bit_loc = bit/32;
	hist[i][0] = hist[i][1];
	hist[j][0] = hist[j][1];
	hist[i][1] = (pop[i][bit_loc] >> bit) & 1;  //get bit'th bit from p1 chrom
	hist[j][1] = (pop[j][bit_loc] >> bit) & 1;
}
//fill population of chromsones with random values and calculate fitness
void fill_pop(int ** pop, int * fit, int * tot_fit, int pop_size)
{
	int i;
	int j;
	for (i=0; i<pop_size; i++)
	{
		for (j=0; j<NUM_INTS; j++)
		{
			pop[i][j] = rand();
		}
	}
}

//select parents of next generation
void selection(int ** pop, int ** pop_next, int *fit, int *tot_fit, int pop_size)
{
	int sel_thresh, count = 0;
	double sel_prob;
	while(count < pop_size)
	{
		sel_thresh = rand() % 100;
		int i;
		for (i=0; i<pop_size; i++)
		{
			sel_prob = ((double)fit[i]/(double)(*tot_fit)) * 100;
			if (sel_prob > sel_thresh)
			{
				pop_next[i] = pop[i];
				count++;
			}
		}		
	}
	int ** temp = pop;
	pop = pop_next;
	pop_next = temp;
}

//crossover chromosones so parents give birth to child
void crossover(int ** pop, int ** pop_next, int pop_size, float crossover_rate)
{
	int * parent1;
	int * parent2;
	int cross_bit, cross_loc, bit, upper_mask, lower_mask, temp1, temp2;
	int i,j;
	for (i=0; i<pop_size; i++)  //POP_SIZE/2 because 2 sets of parents
	{
		if (i < pop_size/2)
		{
			parent1 = pop[i];
			parent2 = pop[i+pop_size/2];
		}

		else  
		{
			parent1 = pop[i];
			parent2 = pop[i-pop_size/2];
		}

		if ((rand() % RAND_MAX) < crossover_rate)
		{
			cross_bit = rand() % STRING_LENGTH; 
			cross_loc = STRING_LENGTH/cross_bit;
			bit = cross_bit = cross_loc*32;
			upper_mask = (int)(pow(2, 32) - 1) >> (32-bit);
			upper_mask = upper_mask << (32-bit);
			lower_mask = (int)(pow(2, 32-bit)) - 1;	
			for (j=0; j<NUM_INTS; j++)
			{
				if (j < cross_loc)
					pop_next[i][j] = parent1[j];
				else if (j > cross_loc)
					pop_next[i][j] = parent2[j];
				else if (j == cross_loc)
				{
					temp1 = parent1[j] & upper_mask;
					temp2 = parent2[j] & lower_mask;
					pop_next[i][j] = temp1 + temp2;
				}
			}		
		}
		else
			pop_next[i] = pop[i];
	}
	int ** temp = pop;
	pop = pop_next;
	pop_next = temp;	
}

//mutate a chromosone by flipping a bit
void mutation(int ** pop, int ** pop_next, int pop_size, float mutation_rate)
{
	int cross_bit, mask, which_int, on_or_off;
	int i;
	for (i=0; i<pop_size; i++)
	{
		if ((rand() % RAND_MAX) < mutation_rate)
		{
			cross_bit = rand() % STRING_LENGTH;
			which_int = cross_bit/32;
			mask = pow(2, cross_bit-which_int*32); 
			on_or_off = mask & pop[i][which_int];
			if (on_or_off == 1)
				pop_next[i][which_int] = mask ^ pop[i][which_int];
			else if (on_or_off == 0)
				pop_next[i][which_int] = mask | pop[i][which_int];
		} 
	}
	int ** temp = pop;
	pop = pop_next;
	pop_next = temp;
}

