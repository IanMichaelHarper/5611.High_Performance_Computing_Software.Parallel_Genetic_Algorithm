#define STRING_LENGTH 244  //chrom length
#define NUM_INTS STRING_LENGTH/32

void fill_pop(int ** pop, int * fit, int *tot_fit, int pop_size);
void selection(int ** pop, int ** pop_next, int *fit, int *tot_fit, int pop_size);
void crossover(int ** pop, int ** pop_next, int pop_size, float crossover_rate);
void mutation(int ** pop, int ** pop_next, int pop_size, float mutation_rate);
void calc_tot_fit(int ** pop, int * fit, int * tot_fit);
void update_hist(int ** hist, int **pop, int i, int j);
void print_pop( int ** pop, int pop_size);
