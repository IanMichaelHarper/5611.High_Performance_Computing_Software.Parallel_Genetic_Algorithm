#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "functions.h"

int main(int argc, char * argv[])
{	
	int pop_size;
	int generations;
	int num_games;
	float crossover_rate;
	float mutation_rate;

	if (argc == 1)
	{
		pop_size = 4;
		generations = 100;
		num_games = 10;
		crossover_rate = 0.8;
		mutation_rate = 0.01;
	}
	else if (argc < 6)
	{
		printf("too few command line arguments passed\nplease run as './<exec> <population size> <number of generations> <number of games of prisoners dilemma to play> <crossover rate> <mutation rate>\n");
		return 1;
	}
	else
	{
		pop_size = atoi(argv[1]);
		generations = atoi(argv[2]);
		num_games = atoi(argv[3]);
		crossover_rate = atof(argv[4]);
		mutation_rate = atof(argv[5]);
	}
	
	srand(time(NULL));

	//initialise variables and allocate memory
	int * tot_fit = malloc(sizeof(int)); 
	int * fit = calloc(pop_size, sizeof(int));
	
	int ** pop = malloc(pop_size * sizeof(int *));
	int * popdata = malloc(pop_size * NUM_INTS * sizeof(int));
	int ** pop_next = malloc(pop_size * sizeof(int *));
	int * pop_nextdata = malloc(pop_size * NUM_INTS * sizeof(int));
	int ** hist = malloc(pop_size * 2 * sizeof(int *));  //create an array storing the result of the last two games for each player
	int * hist_player = malloc(pop_size * sizeof(int));
	int i, j;
	for (i=0; i<pop_size; i++)
	{
		pop[i] = &popdata[NUM_INTS*i];
		pop_next[i] = &pop_nextdata[NUM_INTS*i];
		hist[i] = &hist_player[2*i];
		
	}	

	//reward matrix
	int p1_time_off[][2] = {{3,0},{5,1}};
	int p2_time_off[][2] = {{3,5},{0,1}};

	fill_pop(pop, fit, tot_fit, pop_size);
	//print_pop(pop, pop_size);
	//calc_tot_fit(pop, fit, tot_fit);
	
	//fitness is time off from last game

	int tot_fit_arr[generations];
	int k, g;
	for (k=0; k<generations; k++)
	{
		//calc_tot_fit(pop, fit, tot_fit);
		for (i=0;i<pop_size; i++)
		{
			//reset fitness to 0
			fit[i] = 0;

			//init history from first two games
			hist[i][0] = rand() % 2;
			hist[i][1] = rand() % 2;
			for (j=0;j<i;j++)
			{
				//fitness after 1st game
				fit[i] = p1_time_off[hist[i][0]][hist[j][0]];
				fit[j] = p2_time_off[hist[j][0]][hist[i][0]];
				*tot_fit += fit[i]+fit[j];

				//fitness after second game
				fit[i] = p1_time_off[hist[i][1]][hist[j][1]];
				fit[j] = p2_time_off[hist[j][1]][hist[i][1]];
				*tot_fit += fit[i]+fit[j];
				
				for ( g = 2; g < num_games; g++)
				{	
					update_hist(hist, pop, i, j);  //game is played, history is updated with result

					//calc fitnesss
					fit[i] = p1_time_off[hist[i][1]][hist[j][1]];
					fit[j] = p2_time_off[hist[j][1]][hist[i][1]];
					*tot_fit += fit[i]+fit[j];				
				}
			}
		}
		tot_fit_arr[k] = *tot_fit;		
		
		//gen algo
		selection(pop, pop_next, fit, tot_fit, pop_size);
		crossover(pop, pop_next, pop_size, crossover_rate);
		mutation(pop, pop_next, pop_size, mutation_rate);

		//printf("total fitness = %d\n", *tot_fit);

	}

	FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");
	fprintf(gnuplotPipe, "plot '-' \n");
	for (i = 0; i < generations; i++)
	{
  		fprintf(gnuplotPipe, "%d %d\n", i, tot_fit_arr[i]);
	}
	fprintf(gnuplotPipe, "e");

	//free memory
	free(popdata);
	free(pop);
	free(pop_nextdata);
	free(pop_next);
	free(fit);
	free(tot_fit);

	return 0;
}
