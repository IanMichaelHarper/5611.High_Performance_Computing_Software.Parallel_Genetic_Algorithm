#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <mpi.h>

#include "functions.h"

int main(int argc, char * argv[])
{	
	int size, rank;
	char hostname[256];
	MPI_Status stat;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	gethostname(hostname, 256);

	srand(time(NULL));

	//initialise variables and allocate memory
	//int * tot_fit = malloc(sizeof(int)); //dunno if able to initalize this to 1
	int fit_init = 0;
	int * tot_fit = &fit_init;
	int * fit = calloc(POP_SIZE, sizeof(int));
	*tot_fit = 0; //reassign to 0?
	int ** pop = malloc(POP_SIZE * sizeof(int *));
	int * popdata = malloc(POP_SIZE * NUM_INTS * sizeof(int));
	int ** pop_next = malloc(POP_SIZE * sizeof(int *));
	int * pop_nextdata = malloc(POP_SIZE * NUM_INTS * sizeof(int));
	int ** hist = malloc(POP_SIZE * sizeof(int *));  //create an array storing the result of the last two games for each player
	int * hist_player = malloc(POP_SIZE*2 * sizeof(int));
	int i,j;
	for (i=0; i<POP_SIZE; i++)
	{
		pop[i] = &popdata[NUM_INTS*i];
		pop_next[i] = &pop_nextdata[NUM_INTS*i];
		hist[i] = &hist_player[2*i];
		
	}	

	//player reward matrix
	int p1_time_off[][2] = {{3,0},{5,1}};
	int p2_time_off[][2] = {{3,5},{0,1}};

	//initalise chromsones and first two games
	if( rank == 0) 
	{
		fill_pop(pop, fit, tot_fit);

		for (i=0;i<POP_SIZE; i++)
		{
			//init history from first two games
			hist[i][0] = rand() % 2;
			hist[i][1] = rand() % 2;
			//printf("hist[i][0] = %d\n", hist[i][0]);

			for (j=0;j<i;j++)
			{
				//fitness after 1st game
				fit[i] = p1_time_off[hist[i][0]][hist[j][0]];
				fit[j] = p2_time_off[hist[j][0]][hist[i][0]];
				*tot_fit += fit[i]+fit[j];

				//fitness after second game
				fit[i] = p1_time_off[hist[i][1]][hist[j][1]];
				fit[j] = p2_time_off[hist[j][1]][hist[i][1]];
				*tot_fit += fit[i]+fit[j];
			}
		}
	}

	printf("tot_fit init = %d\n", *tot_fit);
	//print_pop(pop);
	//calc_tot_fit(pop, fit, tot_fit);
	
	//fitness is time off from last game

	int k, g, generations = 100;
	int tot_fit_arr[generations];
	int player, temp_fit=0, temp_tot_fit=0, tag=5;
	for (k=0; k<generations; k++)
	{
		// COLLECTIVE COMMUNICATION ON ALL RANKS
		MPI_Bcast(popdata, POP_SIZE*NUM_INTS, MPI_INT, 0, MPI_COMM_WORLD);
		MPI_Bcast(pop, POP_SIZE, MPI_INT, 0, MPI_COMM_WORLD);
		MPI_Bcast(hist_player, POP_SIZE*2, MPI_INT, 0, MPI_COMM_WORLD);
		MPI_Bcast(hist, POP_SIZE, MPI_INT, 0, MPI_COMM_WORLD);

		if (rank == 0)
		{
			//for (i=0;i<POP_SIZE; i++)
				//fit[i] = 0;
			
			player = 0;
			for( i = 1; i <size ; i ++)
			{
				MPI_Send(&fit[player], 1, MPI_INT, i, player, MPI_COMM_WORLD);
				//send_next_job ( i ) ; // Tell process i to calculate fitness for p0
				player += 1;
			}

			
			while ( player < POP_SIZE )  {
				//get_result ( temp_fit ) ;    // give me the results and update the fitness 
				MPI_Recv(&temp_fit, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
				fit[stat.MPI_TAG] = temp_fit;
				printf("temp_fit = %d\n", temp_fit);
				//MPI_Recv(temp_tot_fit, 1, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &stat);
				*tot_fit += temp_fit; //do I need this since tot_fit is pointer?

				//send_next_job ( P0 ) ;
				//printf("before while send\n");
				MPI_Send(&fit[player], 1, MPI_INT, stat.MPI_SOURCE, player, MPI_COMM_WORLD);		
				//printf("after while send\n");
				player += 1; 
			}

			for( i =1; i <size ; i ++) {
				//get_results ( s ) ; //receive the fitness 
				MPI_Recv(&temp_fit, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
				fit[stat.MPI_TAG] = temp_fit;
				//MPI_Recv(temp_tot_fit, 1, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &stat);
				*tot_fit += temp_fit; //do I need this since tot_fit is pointer?
				
				/*
				for( k = 0; k < POP_SIZE; k++)
				{
					global_fit[k] += temp_fit[k];
				}
				*/
				//no_more_jobs( s ) ; TAG = WE ARE FINISHED
				//printf("before 2nd for send\n");
				MPI_Send(&fit[player], 1, MPI_INT, stat.MPI_SOURCE, tag, MPI_COMM_WORLD);
				//printf("after 2nd for send\n");
			}

			//MPI_Bcast(hist, 2*POP_SIZE, MPI_INT, 0, MPI_COMM_WORLD);

			//MPI_Reduce??
			/*printf("master waiting to receive fit and tot_fit\n");
			MPI_Recv(fit, POP_SIZE, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &stat);
			printf("master received fit\n");
			MPI_Recv(tot_fit, 1, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &stat);
			printf("master received t0t_fit\n");*/

			/*

			int * child1, *child2;
			for( i = 0; i < pop_size/2; i++)
			{
				child1 = selection( pop, fit, tot_fit);
				child2 = selection( pop, fit, tot_fit);				

				crossover( child1, child2);

				mutate(child1);
				mutate(child2);

				pop_next[i][j] = child1;
				pop_next[i+1][j] = child2;
	
				
			}

			swap pop and pop_next but don't need triple pointers. 


			*/

			selection(&pop, &pop_next, fit, tot_fit);

			int * parent1;
			int * parent2;
			//int * parent1 = malloc(sizeof( NUM_INTS));
			//int * parent2 = malloc(sizeof( NUM_INTS)); 
			int cross_bit, cross_loc, bit, upper_mask, lower_mask, temp1, temp2;
			for (i=0; i<POP_SIZE; i++)  //POP_SIZE/2 because 2 sets of parents
			{
				if (i < POP_SIZE/2)
				{
					parent1 = pop[i];
					printf("before bug\n");
					parent2 = pop[i+POP_SIZE/2];
					printf("after bug\n");
					//printf("parent2 = %d\n", *pop[i+POP_SIZE/2][0]);
				}

				else  //should I make this an else if to be specific? and if it doesn't meet that requirement, give an error?
				{
					parent1 = pop[i];
					parent2 = pop[i-POP_SIZE/2];
				}

				if ((rand() % RAND_MAX) < CROSSOVER_RATE)
				{
					cross_bit = rand() % STRING_LENGTH; //is this correct?
					cross_loc = STRING_LENGTH/cross_bit;
					bit = cross_bit = cross_loc*32;
					upper_mask = (int)(pow(2, 32) - 1) >> (32-bit);
					upper_mask = upper_mask << (32-bit);
					lower_mask = (int)(pow(2, 32-bit)) - 1;	
					for (j=0; j<NUM_INTS; j++)
					{
						if (j < cross_loc)
							pop_next[i][j] = parent1[j];
						else if (j > cross_loc)
							pop_next[i][j] = parent2[j];
						else if (j == cross_loc)
						{
							//parents only give birth to one child here, do I need to figure out a way to make 2 children?
							temp1 = parent1[j] & upper_mask;
							temp2 = parent2[j] & lower_mask;
							pop_next[i][j] = temp1 + temp2;
						}
					}		
				}
				else
					pop_next[i] = pop[i];
			}
	
			int ** temp = pop;
			pop = pop_next;
			pop_next = temp;	
			printf("before mutation, after crossover\n");
			//crossover(&pop, &pop_next);
			mutation(&pop, &pop_next);

			//MPI_Send(pop, POP_SIZE*NUM_INTS, MPI_INT, stat.MPI_SOURCE, 0, MPI_COMM_WORLD);  //need to make sure of src
		}
		else
		{
			while(1)
			{
				MPI_Recv(&temp_fit, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
				if (stat.MPI_TAG == tag)
					break;
				
				fit[stat.MPI_TAG] = temp_fit;
				// fit [ i] = 0 	
				i = player;
			
				//round robin
				for (j=0;j<i;j++)
				{
					for ( g = 2; g < NUM_GAMES; g++)
					{	
						update_hist(hist, pop, i, j);  //game is played
						//printf("after update hist\n");

						//calc fitnesss
						fit[i] = p1_time_off[hist[i][1]][hist[j][1]];
						fit[j] = p2_time_off[hist[j][1]][hist[i][1]];
						printf("%d\n", fit[i]);
						//*tot_fit += fit[i]+fit[j];	
						//printf("after fit calc\n");			
					}
				}
				printf("fit[%d] = %d\n", player, fit[player]);			
				MPI_Send(&fit[player], 1, MPI_INT, 0, player, MPI_COMM_WORLD);
				//MPI_Send(tot_fit, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
				//printf("slave %d waiting to receive pop\n", rank); 
				//MPI_Recv(pop, POP_SIZE*NUM_INTS, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
				//printf("slave %d received pop\n", rank);
			}
		}
		tot_fit_arr[k] = *tot_fit;
		//printf("waiting at barrier\n");
		//MPI_Barrier(MPI_COMM_WORLD);

		//printf("total fitness = %d, iteration = %d\n", *tot_fit, k);
	}
	printf("after iter loop\n");
	MPI_Barrier(MPI_COMM_WORLD);

	if (rank == 0)
	{
		FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");
		fprintf(gnuplotPipe, "plot '-' \n");
		for (i = 0; i < generations; i++)
		{
	  		fprintf(gnuplotPipe, "%d %d\n", i, tot_fit_arr[i]);
		}
		fprintf(gnuplotPipe, "e");
	}

	printf("address of pop = %p\n", pop);
	printf("address of pop_next = %p\n", pop_next);
	printf("address of hist = %p\n", hist);
	//free memory
	free(popdata);
	free(pop);
	free(pop_nextdata);
	free(pop_next);
	free(fit);
	//free(tot_fit);
	free(hist_player);
	free(hist);
	printf("after free\n");
	printf("rank = %d\n", rank);

	MPI_Finalize();
	printf("after finalize\n");
	return 0;
}
