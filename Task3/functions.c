#include "functions.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
void print_pop( int ** pop )
{
	int i,j;
	for (i=0; i<POP_SIZE; i++)
	{
		for (j=0; j<NUM_INTS; j++)
		{
			printf("%d ", pop[i][j]);
		}
		printf("\n");
	}
	
}

void calc_tot_fit(int ** pop, int * fit, int * tot_fit)
{
	//fitness is time off from last game
	//fit[0] = p1_time_off[hist[0][1]][hist[0][1]];
	//fit[1] = p2_time_off[hist[1][1]][hist[1][1]];
	//*tot_fit += fit[0]+fit[1];
}

void update_hist(int ** hist, int ** pop, int i, int j)
{
	int bit, bit_loc;
	bit = hist[i][0]*pow(2,3) + hist[j][0]*pow(2,2) + hist[i][1]*2 + hist[j][1]*1;
	bit_loc = bit/32;
	hist[i][0] = hist[i][1];
	hist[j][0] = hist[j][1];
	hist[i][1] = (pop[i][bit_loc] >> bit) & 1;  //get bit'th bit from p1 chrom
	hist[j][1] = (pop[j][bit_loc] >> bit) & 1;
}
//fill population of chromsones with random values and calculate fitness
void fill_pop(int ** pop, int * fit, int * tot_fit)
{
	int i;
	int j;
	for (i=0; i<POP_SIZE; i++)
	{
		for (j=0; j<NUM_INTS; j++)
		{
			pop[i][j] = rand();
		}
	}
}

void selection(int *** pop, int *** pop_next, int *fit, int *tot_fit)
{
	int sel_thresh, count = 0;
	double sel_prob;

	printf("tot_fit = %d\n", *tot_fit);
	int i;
	for (i=0; i<POP_SIZE; i++)
		printf("fit[%d] = %d\n", i, fit[i]);

	while(count < POP_SIZE)
	{
		sel_thresh = rand() % 100;
		for (i=0; i<POP_SIZE; i++)
		{
			sel_prob = ((double)fit[i]/(double)(*tot_fit)) * 100; //selection wheel?
			if (sel_prob > sel_thresh)
			{
				pop_next[i] = pop[i];
				count++;
			}
		}		
	}
	printf("got outside while loop\n");
	int ** temp = *pop;
	*pop = *pop_next;
	*pop_next = temp;
}

void crossover(int *** pop, int *** pop_next)
{
	int * parent1 = malloc(sizeof( NUM_INTS));
	int * parent2 = malloc(sizeof( NUM_INTS)); 
	int cross_bit, cross_loc, bit, upper_mask, lower_mask, temp1, temp2;
	int i,j;
	for (i=0; i<POP_SIZE; i++)  //POP_SIZE/2 because 2 sets of parents
	{
		if (i < POP_SIZE/2)
		{
			//parent1 = **pop + i;
			
			//parent2 = **pop + (i+POP_SIZE/2);
			//printf("parent2 = %d\n", *pop[i+POP_SIZE/2][0]);
			for( j = 0; j < NUM_INTS; j++)
			{
				parent1[j] = *(pop)[i][j];
				printf("parent1 = %d\n", parent1[j] );
				parent2[j] = *(pop)[i + POP_SIZE/2][j];
				printf("parent2 = %d\n", parent2[j] );
			}
		}

		else  //should I make this an else if to be specific? and if it doesn't meet that requirement, give an error?
		{
			parent1 = *pop[i];
			parent2 = *pop[i-POP_SIZE/2];
		}

		if ((rand() % RAND_MAX) < CROSSOVER_RATE)
		{
			cross_bit = rand() % STRING_LENGTH; //is this correct?
			cross_loc = STRING_LENGTH/cross_bit;
			bit = cross_bit = cross_loc*32;
			upper_mask = (int)(pow(2, 32) - 1) >> (32-bit);
			upper_mask = upper_mask << (32-bit);
			lower_mask = (int)(pow(2, 32-bit)) - 1;	
			for (j=0; j<NUM_INTS; j++)
			{
				if (j < cross_loc)
					*pop_next[i][j] = parent1[j];
				else if (j > cross_loc)
					*pop_next[i][j] = parent2[j];
				else if (j == cross_loc)
				{
					//parents only give birth to one child here, do I need to figure out a way to make 2 children?
					temp1 = parent1[j] & upper_mask;
					temp2 = parent2[j] & lower_mask;
					*pop_next[i][j] = temp1 + temp2;
				}
			}		
		}
		else
			pop_next[i] = pop[i];
	}
	
	int ** temp = *pop;
	*pop = *pop_next;
	*pop_next = temp;	
}

void mutation(int *** pop, int *** pop_next)
{
	int cross_bit, mask, which_int, on_or_off;
	int i;
	for (i=0; i<POP_SIZE; i++)
	{
		if ((rand() % RAND_MAX) < MUTATION_RATE)
		{
			cross_bit = rand() % STRING_LENGTH;
			which_int = cross_bit/32;
			mask = pow(2, cross_bit-which_int*32); 
			on_or_off = mask & *(pop[i][which_int]);
			if (on_or_off == 1)
				*pop_next[i][which_int] = mask ^ *(pop[i][which_int]);
			else if (on_or_off == 0)
				*pop_next[i][which_int] = mask | *(pop[i][which_int]);
		} 
	}
	//need to make pop triple pointer
	int ** temp = *pop;
	*pop = *pop_next;
	*pop_next = temp;
}

