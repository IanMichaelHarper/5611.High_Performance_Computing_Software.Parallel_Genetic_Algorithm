#include "functions.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

void print_pop( int ** pop )
{
	int i,j;
	for (i=0; i<POP_SIZE; i++)
	{
		for (j=0; j<NUM_INTS; j++)
		{
			printf("%d ", pop[i][j]);
		}
		printf("\n");
	}
	
}

void calc_tot_fit(int ** pop, int * fit, int * tot_fit)
{
	int i,j;
	for (i=0; i<POP_SIZE; i++)
	{
		fit[i] = 0;
		for (j=0; j<NUM_INTS; j++)
		{
			fit[i] += __builtin_popcount(pop[i][j]);
		}
		*tot_fit += fit[i];
	}
}
//fill population of chromsones with random values and calculate fitness
void fill_pop(int ** pop, int * fit, int * tot_fit)
{
	int i;
	int j;
	*tot_fit = 0;
	for (i=0; i<POP_SIZE; i++)
	{
		for (j=0; j<NUM_INTS; j++)
		{
			pop[i][j] = rand();
			fit[i] += __builtin_popcount(pop[i][j]);
		}
		*tot_fit += fit[i];
	}
}

void selection(int ** pop, int ** pop_next, int *fit, int *tot_fit)
{
	int sel_thresh, count = 0;
	double sel_prob;
	while(count < POP_SIZE)
	{
		sel_thresh = rand() % 100;
		int i;
		for (i=0; i<POP_SIZE; i++)
		{
			sel_prob = ((double)fit[i]/(double)(*tot_fit)) * 100;  //roulette wheel method
			if (sel_prob > sel_thresh)
			{
				pop_next[i] = pop[i];
				count++;
			}
		}		
	}
	int ** temp = pop;
	pop = pop_next;
	pop_next = temp;
}

void crossover(int ** pop, int ** pop_next)
{
	int * parent1;
	int * parent2;
	int cross_bit, cross_loc, bit, upper_mask, lower_mask, temp1, temp2;
	int i,j;
	for (i=0; i<POP_SIZE; i++)  //POP_SIZE/2 because 2 sets of parents
	{
		if (i < POP_SIZE/2)
		{
			parent1 = pop[i];
			parent2 = pop[i+POP_SIZE/2];
		}

		else  
		{
			parent1 = pop[i];
			parent2 = pop[i-POP_SIZE/2];
		}

		if ((rand() % RAND_MAX) < CROSSOVER_RATE)
		{
			cross_bit = rand() % STRING_LENGTH; 
			cross_loc = STRING_LENGTH/cross_bit;
			bit = cross_bit = cross_loc*32;
			upper_mask = (int)(pow(2, 32) - 1) >> (32-bit);
			upper_mask = upper_mask << (32-bit);
			lower_mask = (int)(pow(2, 32-bit)) - 1;	
			for (j=0; j<NUM_INTS; j++)
			{
				if (j < cross_loc)
					pop_next[i][j] = parent1[j];
				else if (j > cross_loc)
					pop_next[i][j] = parent2[j];
				else if (j == cross_loc)
				{
					temp1 = parent1[j] & upper_mask;
					temp2 = parent2[j] & lower_mask;
					pop_next[i][j] = temp1 + temp2;
				}
			}		
		}
		else
			pop_next[i] = pop[i];
	}

	int ** temp = pop;
	pop = pop_next;
	pop_next = temp;	
}

void mutation(int ** pop, int ** pop_next)
{
	int cross_bit, mask, which_int, on_or_off;
	int i;
	for (i=0; i<POP_SIZE; i++)
	{
		if ((rand() % RAND_MAX) < MUTATION_RATE)
		{
			cross_bit = rand() % STRING_LENGTH;
			which_int = cross_bit/32;
			mask = pow(2, cross_bit-which_int*32); 
			on_or_off = mask & pop[i][which_int];
			if (on_or_off == 1)
				pop_next[i][which_int] = mask ^ pop[i][which_int];
			else if (on_or_off == 0)
				pop_next[i][which_int] = mask | pop[i][which_int];
		} 
	}

	int ** temp = pop;
	pop = pop_next;
	pop_next = temp;
}

