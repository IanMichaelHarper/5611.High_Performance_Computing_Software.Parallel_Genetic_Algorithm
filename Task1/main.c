#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "functions.h"

int main(int argc, char * argv[])
{	
	//initialise variables and allocate memory
	int * tot_fit = malloc(sizeof(int)); 
	int * fit = calloc(POP_SIZE, sizeof(int));
	int ** pop = malloc(POP_SIZE * sizeof(int *));
	int * popdata = malloc(POP_SIZE * NUM_INTS * sizeof(int));
	int ** pop_next = malloc(POP_SIZE * sizeof(int *));
	int * pop_nextdata = malloc(POP_SIZE * NUM_INTS * sizeof(int));
	int i;
	for (i=0; i<POP_SIZE; i++)
	{
		pop[i] = &popdata[NUM_INTS*i];
		pop_next[i] = &pop_nextdata[NUM_INTS*i];
	}	

	srand(time(NULL));

	fill_pop(pop, fit, tot_fit);
//	print_pop(pop);

	//gen algo
	int iterations = 100;
	int * tot_fit_arr = malloc(iterations * sizeof(int));
	for (i=0; i<iterations; i++)
	{
		selection(pop, pop_next, fit, tot_fit);
		crossover(pop, pop_next);
		mutation(pop, pop_next);
		calc_tot_fit(pop, fit, tot_fit);
		tot_fit_arr[i] = *tot_fit;
		//printf("total fitness = %d\n", *tot_fit);
	}

	//plot graph
	FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");
	fprintf(gnuplotPipe, "plot '-' \n");
	for (i = 0; i < iterations; i++)
	{
  		fprintf(gnuplotPipe, "%d %d\n", i, tot_fit_arr[i]);
	}
	fprintf(gnuplotPipe, "e");

	//free memory
	free(popdata);
	free(pop);
	free(pop_nextdata);
	free(pop_next);
	free(fit);
	free(tot_fit);
	free(tot_fit_arr);
	return 0;
}
