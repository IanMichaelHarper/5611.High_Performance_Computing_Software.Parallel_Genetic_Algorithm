#define STRING_LENGTH 144  //chrom length
#define NUM_INTS STRING_LENGTH/32
#define POP_SIZE 4
#define MUTATION_RATE 0.01
#define CROSSOVER_RATE 0.8

void fill_pop(int ** pop, int * fit, int *tot_fit);
void selection(int ** pop, int ** pop_next, int *fit, int *tot_fit);
void crossover(int ** pop, int ** pop_next);
void mutation(int ** pop, int ** pop_next);
void calc_tot_fit(int ** pop, int * fit, int * tot_fit);
void print_pop( int ** pop );
